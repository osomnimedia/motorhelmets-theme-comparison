/*
+------------------------+
| Compare Product Script |
| Date: 12/01/16         |
| Author: NJ             |
+------------------------+
*/

jQuery(function($) {
  var my_price_new;
  var my_price;
  var my_price_old;
	var cookieName = "nj_Product_compare_test";
	try {
		if($j.cookie(cookieName) != null && $j.cookie(cookieName) != '_' && $j.cookie(cookieName) != '') {
			var str = String($j.cookie(cookieName)).split("_");
			var count = 0;
			for(var i=0; i<str.length; i++) {
				if(str[i] != '' && str[i] != 'null') {
					count++;
					getCompItems(str[i], count);
				}
			}
		} else {
			getCompItemsEmpty();
		}
	}
	catch (err) {
    	console.log(err);
    } // ignore errors reading cookies
	
	function getCompItems(p, count) {
		var my_result ;
		jQuery.getJSON('/products/'+p+'.js', function(product) {
			var my_image = getCompItemsImageFix(product.featured_image);
			var my_title = product.title.replace(/"/g, "&quot;");
			var colors = product.options[0].values;
			var my_color = '';
			for (i = 0; i < colors.length; ++i) {
			    my_color += colors[i]+', ';
			}
			my_color = my_color.substring(0, my_color.lastIndexOf(', '));
			
			var my_price_min = getCompItemsPrice(product.price_min);
			var my_price_max = getCompItemsPrice(product.price_max);
			var my_compare_at_price_min = getCompItemsPrice(product.compare_at_price_min);
			var my_compare_at_price_max = getCompItemsPrice(product.compare_at_price_max);
			var my_total_color = product.options[0].values.length;
			
			if(product.compare_at_price_min > product.price_min) {
				if(product.price_min == product.price_max) {
					my_price_new = '<span nj="new">'+my_price_min+'</span>';
				} else {
					my_price_new = '<span nj="new">'+my_price_min+' to '+my_price_max+'</span>';
				}
				if(product.compare_at_price_min == product.compare_at_price_max) {
					my_price_old = '<span nj="old">'+my_compare_at_price_min+'</span>';
				} else {
					my_price_old = '<span nj="old">'+my_compare_at_price_min+' to '+my_compare_at_price_max+'</span>';
				}
				my_price = my_price_new+' '+my_price_old;
			} else {
				if(product.price_min == product.price_max) {
					my_price = '<span nj="reg">'+my_price_min+'</span>';
				} else {
					my_price = '<span nj="reg">'+my_price_min+' to '+my_price_max+'</span>';
				}
			}
			
			//BRAND imgs
			var my_brand = product.vendor;
			var brandimg = '';

			if(my_brand == "100%"){	
				brandimg = "100-persent-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "AIRHOLE"){	
				brandimg = "airhole-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "ALIAS"){	
				brandimg = "alias-logo-120x120.jpg";
            } else if(my_brand.toUpperCase() == "ARAI"){	
				brandimg = "arai-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "ARBOR"){	
				brandimg = "arbor-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "ALTAMONT"){	
				brandimg = "altamont-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "ALMOST"){	
				brandimg = "almost-logo2-120x120.jpg";
			} else if(my_brand.toUpperCase() == "ALPINESTARS"){	
				brandimg = "alpinestars-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "ANSWER RACING"){	
				brandimg = "answer-racing-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "AXO"){	
				brandimg = "axo-logo-120x120.jpg";
            } else if(my_brand.toUpperCase() == "BELL"){	
				brandimg = "bell-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "BILLABONG"){	
				brandimg = "billabong-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "BOLLE"){	
				brandimg = "bolle-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "BUFF"){	
				brandimg = "buff-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "CARRERA"){	
				brandimg = "carrera-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "CORTECH"){	
				brandimg = "cortech-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "CREATURE"){	
				brandimg = "creature-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "CROCS"){	
				brandimg = "crocs2-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "CROOKS AND CASTLES"){	
				brandimg = "crooks-and-castles-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "DC"){	
				brandimg = "dc-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "ELECTRIC"){	
				brandimg = "electric-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "ELEMENT"){	
				brandimg = "element-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "ETNIES"){	
				brandimg = "etnies-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "EMERICA"){	
				brandimg = "emerica-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "FAMOUS STARS AND STRAPS"){	
				brandimg = "famous-stars-and-straps-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "FMF"){	
				brandimg = "fmf-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "GLOBE"){	
				brandimg = "globe-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "GMAX"){	
				brandimg = "gmax-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "GUCCI"){	
				brandimg = "gucci-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "HJC"){	
				brandimg = "hjc-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "HOVEN"){	
				brandimg = "hoven-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "INDEPENDENT"){	
				brandimg = "independent-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "JANSPORT"){	
				brandimg = "jansport-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "KLIM"){	
				brandimg = "klim-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "KR3W"){	
				brandimg = "kr3w-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "LOST"){	
				brandimg = "lost-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "LRG"){	
				brandimg = "lrg-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "METAL MULISHA"){	
				brandimg = "metal-mulisha-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "MATIX"){	
				brandimg = "matix-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "NEFF"){	
				brandimg = "neff-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "OAKLEY"){	
				brandimg = "oakley-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "OGIO"){	
				brandimg = "ogio-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "O'NEILL"){	
				brandimg = "oneill-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "ORIGINBOARDSHOP"){	
				brandimg = "originboardshop-120x120.jpg";
			} else if(my_brand.toUpperCase() == "PENNY"){	
				brandimg = "penny-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "PUREGREEN24"){	
				brandimg = "puregreen24-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "PSD UNDERWEAR"){	
				brandimg = "psd-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "QUIKSILVER"){	
				brandimg = "quiksilver-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "REEF"){	
				brandimg = "reef-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "REVO"){	
				brandimg = "revo-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "ROXY"){	
				brandimg = "roxy-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "RIP CURL"){	
				brandimg = "rip-curl-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "RUSTY"){	
				brandimg = "rusty-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "SANTA CRUZ"){	
				brandimg = "santa-cruz-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "SANUK"){	
				brandimg = "sanuk-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "SAXX"){	
				brandimg = "saxx-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "SCOTT"){	
				brandimg = "scott-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "SECTOR 9"){	
				brandimg = "sector-9-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "SEVEN"){	
				brandimg = "seven-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "SMITH OPTICS"){	
				brandimg = "smith-optics-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "SPEED AND STRENGTH"){	
				brandimg = "speed-and-stregth-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "SPY OPTIC"){	
				brandimg = "spy-optics-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "STANCE"){	
				brandimg = "stance-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "SUNCLOUD OPTICS"){	
				brandimg = "suncloud-optics-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "SUPRA"){	
				brandimg = "supra-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "TROY LEE DESIGNS"){	
				brandimg = "troy-lee-designs-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "VONZIPPER"){	
				brandimg = "vonzipper-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "FIVE"){	
				brandimg = "five-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "BOBSTER"){	
				brandimg = "bobster-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "SHOT"){	
				brandimg = "shot-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "MOTION PRO"){	
				brandimg = "motion-pro-log-120x120.png";
			} else if(my_brand.toUpperCase() == "ZOX"){	
				brandimg = "zox-120x120.jpg";
			} else if(my_brand.toUpperCase() == "KURYAKIN"){	
				brandimg = "kuryakyn-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "ROLAND SANDS DESIGN"){	
				brandimg = "roland-sands-design-120x120.jpg";
			} else if(my_brand.toUpperCase() == "ODI"){	
				brandimg = "odi-logo-120x120.jpg";   
			} else if(my_brand.toUpperCase() == "PIVOT PEGZ"){	
				brandimg = "pivot-pegz-logo-120x120.png";
			} else if(my_brand.toUpperCase() == "PIVOT PEGZ"){	
				brandimg = "pivot-pegz-logo-120x120.png";
			} else if(my_brand.toUpperCase() == "POD"){	
				brandimg = "pod-mx-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "POD"){	
				brandimg = "pod-mx-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "RICKS"){	
				brandimg = "ricks-logo-120x120.jpg";
			} else if(my_brand.toUpperCase() == "SKID LID"){	
				brandimg = "skid-lid-logo.jpg";
			} else if (my_brand.toUpperCase() == "DARKSTAR") {
				brandimg = "darkstar-logo-120x120.jpg";
			}

			brandimg = '<img class="img-responsive" src="//cdn.shopify.com/s/files/1/1223/6434/files/'+brandimg+'" />';

			var my_description = product.description;
			var my_url = product.url;
			var my_handle = product.handle;
			var my_availability = (product.available) ? '<span nj="in-stock">In Stock</span>' : '<span nj="out-of-stock">Out of Stock</span>';
			var my_action = '<label><input type="checkbox" nj-check="" /></label>';

			var sampleimg = product.images;
			var c = 0;
			var d = product.images.length/colors.length;
			var imgs = '<div class="jubs_compare_product_list"><ul class="slider">';

			for (var i = 0; i <= sampleimg.length; i++) {
				
				if (sampleimg[c] != null && sampleimg[c] != '' && i == c) {
					imgs += '<li><img src="'+getCompItemsThumbImage(sampleimg[i])+'" title="'+my_title+'" alt="'+my_title+'"/></li>';
					c += d;
				}	
				
			}
          			
			imgs += '</ul></div>';
          
          
          	//Image Variants
            var images = [];
            for (var i = 0; i < product.variants.length; i++) {
                images.push('<div><img src="' + product.variants[i].featured_image.src.replace(/\.jpg/,'_thumb\.jpg') + '" class="'+my_handle+'" title="'+product.variants[i].option1+'" alt="'+product.variants[i].option1+'" /></div>');
            }
            images = '<div class="jubs_compare_product_list">' + removeDupsArray(images).join('') + '</div>';

            var varimages = '<div class="jbn_wishlist_view_images"><div class="nj_recently_view_slider"><div class="nj_recently_view_flex">'+
                 images+ '</div></div></div>';  
          
          
          	var samp = product.images.length;
          	var samps = colors.length;


			my_image = '<a href="javascript:void(0);" title="'+my_title+'"><img src="'+my_image+'" title="'+my_title+'" alt="'+my_title+'"/></a>';
			my_title = '<a href="javascript:void(0);" title="'+my_title+'" onclick="quiqview(\''+my_handle+'\')">'+my_title+'</a>';
			//my_title = '<a href="'+my_url+'" title="'+my_title+'">'+my_title+'</a>';
			my_brand = '<div>'+brandimg+'</div><span>'+my_brand+'</span>';
			my_total_color = '<span>'+my_total_color+'</span>';
			var my_functions = '<div class="default-wishbutton defbtn'+my_handle+'">'+
                            '<a class="add-in-wishlist-js" href="javascript:void(0)" my-handle=\''+my_handle+'\' title="ADD TO WISHLIST">'+
                                '<span class="icon icon-favorite_border"></span> '+
                            '</a>'+
                        '</div>'+
                        '<div class="added-wishbutton" style="display: none;">'+
                           ' <a href="/pages/wishlist"  title="Go to Wishlist">'+
                                '<span class="icon icon-favorite"></span>'+
                            '</a>'+
                        '</div>'+
                		'<div><a href="javascript:void(0);" onclick="quiqview(\''+my_handle+'\')" title="QUICK VIEW">'+
                		'<span class="simple-icon simple-icon-eye"></span></a></div>'+
                		'<div> <a style="display:block;" href="javascript:void(0);" data-handle="'+my_handle+'" nj="remove" title="REMOVE THIS PRODUCT"><span class="icon icon-delete"></span></a></div>';


			my_result = '<div><div class="content-product-compare">'+
							'<div class="comp-func">'+my_functions+'</div>'+
							'<div class="comp-img">'+my_image+'</div>'+images+
							'<div class="comp-ptitle">'+my_title+'</div>'+
							'<div class="comp-price"><b>'+my_price+'</b></div>'+
							'<div class="comp-brand">'+my_brand+'</div>'+
							// '<div class="comp-color">'+my_color+'</div>'+
							'<div class="comp-desc">'+my_description+'</div>'+
							'</div></div>';


			$('.compare-container').slick('slickAdd', my_result);
			getCompItemsEmpty();
			$('.compare-container').slick('refresh');

			getCompItemsEmpty();
          	$(window).trigger("initWishlistNoCustomer");
		}).error(function() {
			getCompItemsRemove(p);
			getCompItemsEmpty();
        });
	}

	//Checkbox
    $(document).on("click", '.nj_products_comparison_table tr td[nj="action"] div label input', function(event) { 
		getCompItemsBtn($('.nj_products_comparison_table tr td[nj="action"] div label input').is(':checked'));
		if($(this).attr('nj-check') == '') {
			$(this).attr('nj-check','ok');
		} else {
			$(this).attr('nj-check','');
		}
	});
	
	//Reset Button
    $(document).on("click", '.nj_products_comparison_table div[nj="filter"] button[nj="btn1"][nj-check="ok"]', function(event) { 
		$('.nj_products_comparison_table tr td[nj="action"] div label input').prop('checked', false);
		getCompItemsBtn($('.nj_products_comparison_table tr td[nj="action"] div label input').is(':checked'));
		$('.nj_products_comparison_table tr td[nj="action"] div label input').attr('nj-check','');
		$('.nj_products_comparison_table tr td[nj="action"] div label input').parent().parent().parent().parent().show();
		$("table .col1").show();
		$("table .col2").show();
		$("table .col3").show();
		$('.nj_products_comparison_table tr td[nj="action"] div label input').prop("disabled",false);
	});
	
	//Filter Button
    $(document).on("click", '.nj_products_comparison_table div[nj="filter"] button[nj="btn2"][nj-check="ok"]', function(event) { 
    	if(!$('.nj_products_comparison_table tr td[class="col1"] div label input').is(':checked')) {
    		var column = "table ." + $('.nj_products_comparison_table tr td[class="col1"]').attr("class");
    		$(column).hide();
    	}
    	if(!$('.nj_products_comparison_table tr td[class="col2"] div label input').is(':checked')) {
    		var column = "table ." + $('.nj_products_comparison_table tr td[class="col2"]').attr("class");
    		$(column).hide();
    	}
    	if(!$('.nj_products_comparison_table tr td[class="col3"] div label input').is(':checked')) {
    		var column = "table ." + $('.nj_products_comparison_table tr td[class="col3"]').attr("class");
    		$(column).hide();
    	}
		//var column = "table ." + $('.nj_products_comparison_table tr td[nj="action"] div label input:not([nj-check="ok"])').parent().parent().parent().attr("class");
		//$(column).hide();
		$('.nj_products_comparison_table tr td[nj="action"] div label input').prop("disabled",true);
	});
	
	//Remove Button
    $(document).on("click", '.nj_products_comparison_table tr td[nj="functions"] div a[nj="remove"]', function(event) { 
		// $(this).parent().parent().parent().fadeOut(300, function(){ $(this).remove();});
		//$(this).parent().parent().parent().remove();
		var columns = $(this).parent().parent().parent().attr("class");
		$(".nj_products_comparison_table tr td[class='"+columns+"']").remove();
		getCompItemsRemove($(this).attr('handle'));
		getCompItemsEmpty();
	});

	$(document).on("mouseover", '.jubs_compare_product_list div div div img', function (event) {
    	var getparent = $(this).parent().parent().parent().parent().parent().parent().attr('data-slick-index');
    	//$(".nj_products_comparison_table tr td[class='"+getclass+"']").remove();
		var hoverimg =  $(this).attr('src');
    	$('.slick-slide[data-slick-index="'+getparent+'"] .content-product-compare .comp-img img').attr("src",ThumbtoMedImage(hoverimg));
	});

  	$(document).on("click", '.comp-func a[nj="remove"]', function(event) {
      
      var product_handle = $(this).attr('data-handle');

      //remove from slick
      var rem = $(this).parent().parent().parent().parent().attr('data-slick-index');

      $('.compare-container').slick('slickRemove', rem).slick('refresh');
      
      //remove from cookie
      getCompItemsRemove(product_handle);

      getCompItemsEmpty();

    });
	
	function getCompItemsRemove(p) {
		var ck = $j.cookie('nj_Product_compare_test');
		var np = p+'_';
			ck = ck.replace(np, '');
			ck = ck.replace('__', '_');
			$j.cookie('nj_Product_compare_test', ck, {expires:7, path:'/'});
	}
	
	function getCompItemsEmpty() {
		if($('.compare-container .slick-track .slick-slide').length) {
			$('.nj_products_comparison_table').show();
			$('.nj_products_comparison').hide();
		} else {
			$('.nj_products_comparison_table').hide();
			$('.nj_products_comparison').show();
		}
	}

	function getCompItemsBtn(c) {
		if(c) {
			$('.nj_products_comparison_table div[nj="filter"] button[nj="btn1"]').attr('nj-check','ok');
			$('.nj_products_comparison_table div[nj="filter"] button[nj="btn2"]').attr('nj-check','ok');
		} else {
			$('.nj_products_comparison_table div[nj="filter"] button[nj="btn1"]').attr('nj-check','');
			$('.nj_products_comparison_table div[nj="filter"] button[nj="btn2"]').attr('nj-check','');
		}
	}
	function getCompItemsImageFix(img) {
		img = img.replace(/\.jpg/g,'_large.jpg');//_small
		img = img.replace(/\.jpeg/g,'_large.jpeg');//_small
      	return img;
	}
	function getCompItemsThumbImage(img) {
		img = img.replace(/\.jpg/g,'_thumb.jpg');//_small
		img = img.replace(/\.jpeg/g,'_thumb.jpeg');//_small
      	return img;
	}
  	function ThumbtoMedImage(img) {
		img = img.replace(/\_thumb.jpg/g,'_large.jpg');//_small
		img = img.replace(/\_thumb.jpeg/g,'_large.jpeg');//_small
      	return img;
	}
	
	
	function getCompItemsPrice(d) {
		return Shopify.formatMoney(d, "${"+"{amount}}");
	}
  
  	function removeDupsArray(data) {
		return data.filter(function(nj,i,data){
			return i == data.indexOf(nj);
		});
	}

	function instVarimg(){
		$j('.jubs_compare_product_list').slick({
			infinite: true,
			dots: true,
			speed: 500,
			slidesToShow: 3,
			slidesToScroll: 3,
			responsive: [{
				breakpoint: 1770,
				settings: {
			        slidesToShow: 3,
			        slidesToScroll: 3
					}
				},{
				breakpoint: 1199,
				settings: {
			        slidesToShow: 3,
			        slidesToScroll: 3
					}
				},{
				breakpoint: 768,
				settings: {
			        slidesToShow: 2,
			        slidesToScroll: 2
					}
				}, 
				{
			    breakpoint: 480,
			    settings: {
			        slidesToShow: 1,
			        slidesToScroll: 1
			    }
			}]
		});
	}

	$j('.compare-container').slick({
        infinite: true,
		dots: true,
		speed: 500,
		slidesToShow: 4,
		slidesToScroll: 2,
		swipeToSlide : true,
		responsive: [{
			breakpoint: 1770,
			settings: {
		        slidesToShow: 4,
		        slidesToScroll: 2
				}
			},{
			breakpoint: 1199,
			settings: {
		        slidesToShow: 3,
		        slidesToScroll: 2
				}
			},{
			breakpoint: 768,
			settings: {
		        slidesToShow: 2,
		        slidesToScroll: 1
				}
			}, 
			{
		    breakpoint: 480,
		    settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		    }
        }]
	});


	setTimeout(instVarimg, 2000);

	$('.compare-container').slick('slickRemove', 0).slick('refresh');
  
});
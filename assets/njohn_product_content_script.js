jQuery(function($) {

  
  
$j('.njohn_clr_var_selector li.njohn_clr_var_image').click(function() {
	$j('.njohn_clr_var_selector li.njohn_clr_var_image').removeClass('davz_color_selected');
	$j(this).addClass('davz_color_selected');
});

 //DISPLAY MAIN IMAGE ON LOAD
 // $j('.njohn_smallGallery ul li:first img:first').css('display','block!important');
// $j('.njohn_smallGallery ul li').addClass('njohn_thumbs_active');
// $j('.njohn_smallGallery ul li img:first').addClass('njohn_img_active');
// $j('.njohn_smallGallery ul li:first img').css('display','block!important');
// $j('.njohn_clr_var_image img').addClass('njohn_img_active');


// ------------------------ this line works ---------------------------------
// var firstThumbnail_id = $j('.njohn_smallGallery ul li:first').attr('id');

// $j('.njohn_smallGallery ul li').each(function() {

// 	if($j(this).attr('id') == firstThumbnail_id)
// 	{
// 		$j(this).addClass('njohn_thumbs_active');
// 	}

// });
// ------------------------ this line works ---------------------------------


	// $j(window).ready(function(){


	// });  


// COLOR LABEL STYLE AT FIRST RELOAD
$j('#njohn_clr_label').html('PLEASE SELECT COLOR');
$j('#njohn_clr_label').css('color' , '#c0392b');

// DISABLE BUTTONS AT FIRST RELOAD
$j('.sold_out_btn').css('display','block').text("Select a Color");
$j('.disabled_buy_btn').css('display','block');
$j('.addtocart').css('display','none');

// IMAGE SELECTOR TRIGGER FUNCTION
$j('.njohn_clr_var_image img').click(function() {
	var attr_title = $j(this).attr('title');
	var attr_id = $j(this).attr('id');
	
	// $j('.defaultGallery').hide();

    // COLOR LABEL STYLE
	$j('#njohn_clr_label').html(attr_title);
	$j('#njohn_clr_label').css('color' , 'black');
	
  	// SIZE LABEL STYLE
	$j('#njohn_left_label').css('color' , '#c0392b');
	$j('#njohn_left_label').html('PLEASE SELECT SIZE');
	
  	// DISPLAY SOLD OUT BUTTON
	$j('.sold_out_btn').css('display','block').text("Select a Size");
  $j('.disabled_buy_btn').css('display','block');
	
  	// HIDE ADD TO CART BUTTON
	$j('.addtocart').css('display','none');
	
  	// UNCHECK SIZES RADIO BUTTON
	$j('.njohn_selector_hide label input').prop('checked', false);
	
  	// REMOVE CLASS FOR SIZES RADIO BUTTON
	$j('.njohn_selector_hide label span').removeClass("start-up-disabled");
	
  	// DISPLAY OR HIDE OTHER VIEWS THOSE AREN'T SELECTED COLOR IMAGE THUMBNAIL
	if ($j('.njohn_smallGallery ul li#' + attr_id).hasClass('njohn_thumbs_active') == false) {
		$j('.njohn_smallGallery ul li').removeClass('njohn_thumbs_active');
		$j('.njohn_smallGallery ul li#' + attr_id).addClass('njohn_thumbs_active');
	}
  
	//ADD CLASS COLOR IMAGE THUMBNAIL SELECTOR
	if ($j('.njohn_clr_var_image img#' + attr_id).hasClass('njohn_img_active') == false) {
		$j('.njohn_clr_var_image img').removeClass('njohn_img_active');
		$j('.njohn_clr_var_image img#' + attr_id).addClass('njohn_img_active');
	}
  
	//TRIGGER TO MAIN IMAGE
	var img_1024x1024 = $j(this).attr('img-1024x1024');
	$j('.zoomContainer .zoomWindowContainer div').css('background-image', 'url(' + img_1024x1024 + ')');
    $j('.product-main-image .product-main-image__item img#bigImg').attr('src', img_1024x1024);
    $j('.product-main-image .product-main-image__item img#bigImg').attr('zoom-image', img_1024x1024);
  
    let img_small = $j(this).attr('src');
    $j('#pickup-info-img').attr('src',img_small);
    let color = $j(this).data('color');
    $j('#pickup-info-color b').text(color);
});
  
    $j('.njohn_smallGallery img').click(function() {
      //TRIGGER TO MAIN IMAGE
      var img_1024x1024 = $j(this).attr('img-1024x1024');
      $j('.zoomContainer .zoomWindowContainer div').css('background-image', 'url(' + img_1024x1024 + ')');
      $j('.product-main-image .product-main-image__item img#bigImg').attr('src', img_1024x1024);
      $j('.product-main-image .product-main-image__item img#bigImg').attr('zoom-image', img_1024x1024);
      
      var attr_id = $j(this).attr('id');
	//ADD CLASS COLOR IMAGE THUMBNAIL SELECTOR
	if ($j(this).hasClass('njohn_img_active') == false) {
		$j('.njohn_smallGallery img').removeClass('njohn_img_active');
		$j(this).addClass('njohn_img_active');
	}
      
    });

// SIZE SELECTOR TRIGGER FUNCTION
$j('.njohn_selector_hide label span').click(function() {

   // SHOW IF THE SPECIFIC SIZE VARIANT REMAINING SMALL AMOUNT OF STOCK
	var only_left_stock = $j(this).attr('only-left-stock');
	
	if($(this).hasClass("start-up-disabled")) {
		$j(".njohn_clr_var_label").effect("shake");
	}
	else
	{
		if(only_left_stock == 'IN STOCK') {
			$j('#njohn_left_label').html(only_left_stock);
			$j('#njohn_left_label').css('color' , 'black');
		}
		else
		{
			$j('#njohn_left_label').html(only_left_stock);
			$j('#njohn_left_label').css('color' , '#c0392b');
		}
	}
	
	
  	// CHECK IF COLOR HAS CLICKED
	var var_availability = $j(this).attr('var-availability');
	if($j(this).hasClass('start-up-disabled') == false) {
      
    	// STATEMENT FOR ZERO STOCK OR AVAILABLE
		if(var_availability == 'in_stock') {
			$j('.sold_out_btn').css('display','none');
          	$j('.disabled_buy_btn').css('display','none');
			$j('.addtocart').css('display','block');
		}
		else
		{
			$j('.sold_out_btn').css('display','block');
          	$j('.disabled_buy_btn').css('display','block');
			$j('.addtocart').css('display','none');
		}
      
        // MSRP/PRICES/SKU
        var compare_at_price = $j(this).attr('compare_at_price');
        var price = $j(this).attr('price');
        var sku = $j(this).attr('sku');

        $j('#njohn_product_price_result').html(priceFilter(price,compare_at_price));
        $j('#njohn_prod_skus').html(sku);
	}
  
  let pickupList = $j('#pickup-img').data('pickupsku');
  let pickupOnlyList = $j('#pickup-img').data('pickuponlysku');
  $j('#pickup-img').removeClass('hide');
  let stockleft = $j(this).data('stockleft');
  if((pickupList.includes(sku) || pickupOnlyList.includes(sku)) && stockleft>0){
    $j('#pickup-img').show();
    $j('#pickup-with-stock').show();
    $j('#pickup-no-stock').hide();
    if(pickupList.includes(sku)){
//       $j('#pickup-only-text').hide();
//       $j('#pickup-eligible-text').show();
    } else if(pickupOnlyList.includes(sku)){
//       $j('#pickup-only-text').show();
//       $j('#pickup-eligible-text').hide();
      $j('#pick-up-now').show();
      $j('.buy-now-row').hide();
    }
  } else {
    $j('#pickup-img').hide();
    $j('#pickup-with-stock').hide();
    $j('#pickup-no-stock').show();
    $j('#pick-up-now').hide();
    $j('.buy-now-row').show();
  }

  let size = $j(this).text();
  $j('#pickup-info-size b').text(size);
  $j('#pickup-info-price b').text(price);

  
});

  function priceFilter(p1,p2) {
    var jbn_price1 = p1.replace("$", "").replace(",", "");
    var jbn_price2 = p2.replace("$", "").replace(",", "");
    jbn_price1 = parseFloat(jbn_price1).toFixed(2);
    jbn_price2 = parseFloat(jbn_price2).toFixed(2);

    var jbn_amt_saved = jbn_price2 - jbn_price1;
    jbn_amt_saved = jbn_amt_saved.toFixed(2);


    var jbn_perc = jbn_price2 - jbn_price1;
    var jbn_totalperc = jbn_perc/jbn_price2;
    jbn_totalperc = jbn_totalperc*100;


    if(p2 == '') {
      var data = '<div class="njohn_price-box">'+
          '<span class="njohn_price3">'+ p1 +'</span>'+
          '</div>';
    }
    else
    {
      var data = '<div class="njohn_price-box">'+
          '<span class="njohn_price2">'+ p1 +'</span> <span class="njohn_price1">'+ p2 +'</span>'+
          '</div>' + '<span class="davz_discount_saved"> You save <span class = "jbn_amt_saved">$' + jbn_amt_saved +' ('+Math.round(jbn_totalperc)+'%) </span></span>';
    }
    return data;
  }

// RETURN PRODUCT VARIANT VALUE TO THE MAIN SELECTOR
$j('.njohn_selector_hide input').click('change', function() {
  $j('#product-select').val(this.value);
  $j('#pickup-button, #pick-up-now button').data('variantid',this.value);
  let stockleft = $j(this).next().data('stockleft');
  $j('.qty-input').attr('max',stockleft);
});

// REMOVE EMPTY SIZE SELECTORS
$j('.njohn_selector .njohn_selector_hide').each(function() {
	if($j(this).html().replace(/\s|&nbsp;/g, '').length == 0)
	$j(this).remove();
});
  
  	$j(window).ready(function(){
		$j('.njohn_clr_var_selector').flexslider({
			animation: "slide",
			itemWidth: 105,
			itemMargin: 10,
            maxItems: (window.innerWidth <= 1770) ? 8 : 8,
            //maxItems: (window.innerWidth <= 1024) ? 6 : 6;,
            animationLoop: true, // Infinity Next
            controlNav: false, // hide control navigator if 'false'
          	slideshow: false, // disable auto play if 'false'
          	// directionNav: true // to disable next & previous buttons
    //       	start: function() {
		  //      // jQuery('.flex-direction-nav').each(function(index){
		          
		  //       jQuery(this).find('ul.slides li:empty').remove();
		         
		  //         if (jQuery(this).find('ul.slides li').length == 1) {
		  //                jQuery(this).find('ul.slides li:empty').remove();
		  //               jQuery(this).find('.flex-direction-nav').remove();
		  //          }
				// // });
		       
		  
		  //   } 
		});
	});  

  	$j(window).ready(function(){
		$j('.njohn_other_prod').flexslider({
			animation: "slide",
			itemWidth: 500,
			itemMargin: 10,
            animationLoop: true, // Infinity Next
            controlNav: false, // hide control navigator if 'false'
          	slideshow: false // disable auto play if 'false'
		});
	});
  
});

// THIS FUNCTION IS IMAGE COLOR THUMBNAIL TRIGGER, IT DISPLAY SPECIFIC SIZES OPTION.
function tabs(evt, tabname) {
	var i, njohn_tabs, tablinks;
	njohn_tabs = document.getElementsByClassName("njohn_selector_hide");
	for (i = 0; i < njohn_tabs.length; i++) {
	njohn_tabs[i].style.display = "none";
	}
	document.getElementById(tabname).style.display = "block";

}

$j('#pickup-qty').on('change', function(e){
  $j('.product-info .qty-input').val(this.value);
});
$j('.product-info .qty-input').on('change', function(e){
  $j('#pickup-qty').val(this.value);
});

$j('#pickup-qty, .product-info .qty-input').on('keyup', function(e){
  let qty = parseInt($j(this).val());
  let max = parseInt($j(this).attr('max'));
    console.log(qty,max);
  if ( qty > max ) {
    $j(this).val(max);
  }
});


// ADD TO CART LOADING BAR
[].slice.call( document.querySelectorAll( 'button.progress-button' ) ).forEach( function( bttn ) {
new ProgressButton( bttn, {
callback : function( instance ) {
var progress = 0,
interval = setInterval( function() {
progress = Math.min( progress + Math.random() * 0.1, 1 );
instance._setProgress( progress );

if( progress === 1 ) {
instance._stop(1);
clearInterval( interval );
}
}, 200 );
}
});
});
$j(document).ready(function (e) {
$j('.njohn_clr_var_selector').find('.flex-direction-nav').addClass('show-flex-direction-nav');
});
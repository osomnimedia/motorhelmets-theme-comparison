/* override functions api.jquery.js */
Shopify.onItemAdded = function(line_item) {
  Shopify.getCart();
};

Shopify.onCartUpdate = function(cart) {
  Shopify.cartUpdateInfo(cart, '.cart-group-1 ul');
};

Shopify.onError = function(XMLHttpRequest, textStatus) {
  var data = eval('(' + XMLHttpRequest.responseText + ')');
  if (!!data.message) {
    var str = data.description;
//     if(data.status == 422) var str = data.description;
    
  } else {
   	var str = 'Error : ' + Shopify.fullMessagesFromErrors(data).join('; ') + '.';
  }
  
  let case1 = str.match(/You can only add [0-9]+/ig);
  if(case1 != null){
    console.log(case1[0]);
    let qty = case1[0].match(/[0-9]+/g);
    let isare = (qty > 1) ? "are" : "is";
    str = "There "+isare+" only "+qty+" available to order.";
  }
  
  let case2 = str.match(/All [0-9]+/ig);
  if(case2 != null){
    let qty = case2[0].match(/[0-9]+/g);
    str = "All stocks for this product is already in your cart.";
  }
  // You can only add 1 100% Accuri Adult Off-Road Goggles - Maneuver / Mirror Blue / One Size to the cart.
  // All 1 Fox Racing Titan Race Elbow Guard Youth Off-Road Body Armor (LIGHTLY USED) - Black / One Size are in your cart.
  // there are only x in stock or more than x is not available to order
  jQuery('#modalAddToCartError .error_message').text(str);
  jQuery('#modalAddToCartError').modal("show");
}

Shopify.addItem = function(variant_id, quantity, callback) {
  var quantity = quantity || 1;
  var params = {
    type: 'POST',
    url: '/cart/add.js',
    data: 'quantity=' + quantity + '&id=' + variant_id,
    dataType: 'json',
    success: function(line_item) {
      if ((typeof callback) === 'function') {
        callback(line_item);
      }
      else {
        Shopify.cartPopap(line_item.title,line_item.image);
        Shopify.onItemAdded(line_item);
      }
    },
    error: function(XMLHttpRequest, textStatus) {
      Shopify.onError(XMLHttpRequest, textStatus);
    }
  };
  jQuery.ajax(params);
};

Shopify.addItemFromForm = function(form_id, variant_id,callback) {
	var params = {
		type: 'POST',
		url: '/cart/add.js',
		beforeSend: function() {
			if(form_id == "nj_qv_add_item_qv") {
				jQuery('#' + form_id).find(".nj_qv_addtocartqv button").text('please wait..');
			}
		},
		data: jQuery('#' + form_id).serialize(),
		dataType: 'json',
		success: function(line_item) {
			if ((typeof callback) === 'function') {
				callback(line_item);
			}
			else {
				if(form_id != "nj_qv_add_item_qv") {
					Shopify.cartPopapForm(line_item.title,line_item.image);
				} else {
					jQuery('#' + form_id).find(".nj_qv_addtocartqv button").text('success..');
					setTimeout(function() {
						jQuery('#' + form_id).find(".nj_qv_addtocartqv button").text('add to cart');
					}, 3000);
				}
				Shopify.onItemAdded(line_item);
			}
		},
		error: function(XMLHttpRequest, textStatus) {
			if(form_id != "nj_qv_add_item_qv") {
				Shopify.onError(XMLHttpRequest, textStatus);
			} else {
				jQuery('#' + form_id).find(".nj_qv_addtocartqv button").text('already added..');
				setTimeout(function() {
					jQuery('#' + form_id).find(".nj_qv_addtocartqv button").text('add to cart');
				}, 3000);
			}
			Shopify.onItemAdded(XMLHttpRequest, textStatus);
		}
	};
	jQuery.ajax(params);
};

/* user functions */

Shopify.addItemFromFormStart = function(form, product_id) {
  Shopify.addItemFromForm(form, product_id);
}

Shopify.cartPopap = function(titlenihuh,imagess) {
    jQuery('#njohn_addtocart_name .njohn_addtocart_cell').html(titlenihuh);  
  	imagess = imagess.replace(/\.jpg\?v=/gi, '_small\.jpg\?v=');
    jQuery('#njohn_addtocart_img').html('<img src="'+imagess+'">');  
    jQuery('#modalAddToCart').modal("show");
}
Shopify.cartPopapForm = function(title,imagess) {
    jQuery('#njohn_addtocart_name .njohn_addtocart_cell').html(title); 
  	imagess = imagess.replace(/\.jpg\?v=/gi, '_small\.jpg\?v='); 
    jQuery('#njohn_addtocart_img').html('<img src="'+imagess+'">');  
    jQuery('#modalAddToCart').modal("show");
}

Shopify.cartUpdateInfo = function(cart, cart_cell_id) {
  let pickuponly = jQuery('#cart-menu-container').data('pickuponly').split(',');
  var formatMoney = "<span class='money'>${{amount}}</span>";
  if ((typeof cart_cell_id) === 'string') {
    var cart_cell = jQuery(cart_cell_id);
    if (cart_cell.length) {

      cart_cell.empty();

      let hide_checkout = false;
      jQuery.each(cart, function(key, value) {
        if (key === 'items') {
			
          if (value.length) {
            jQuery(".header-cart .dropdown-toggle").css({"cursor": "pointer"});
            
            var table = jQuery(cart_cell_id);
            jQuery.each(value, function(i, item) {
              if(pickuponly.includes(item.sku)){
                hide_checkout = true;
              }
              if(i > 19){
                  return false;
              }
              jQuery('<li class="cart__item">'+
			'<div class="cart__item__image pull-left">'+
				'<a href="' + item.url + '">'+
					'<img src="' + item.image + '" alt=""/>'+
				'</a>'+
			'</div>'+
			'<div class="cart__item__control">'+
				'<div class="cart__item__delete">'+
					'<a href="javascript:void(0);" onclick="Shopify.removeItem(' + item.variant_id + ')" class="icon icon-delete">'+
						'<span>' + jQuery(".cart_messages .delete").text() + '</span>'+
					'</a>'+
				'</div>'+
				'<div class="cart__item__edit">'+
					'<a href="' + item.url + '" class="icon icon-edit">'+
						'<span>' + jQuery(".cart_messages .edit").text()+'</span>'+
					'</a>'+
				'</div>'+
			'</div>'+
			'<div class="cart__item__info">'+
				'<div class="cart__item__info__title">'+
					
						'<a class="product_title-CartDpdown" href="' + item.url + '">' + item.title + '</a>'+
					
				'</div>'+
				'<div class="cart__item__info__price">'+
					'<span class="info-label">' + jQuery(".cart_messages .price").text() + '</span>'+
					'<span>' + Shopify.formatMoney(item.price, formatMoney) + '</span>'+
				'</div>'+
				'<div class="cart__item__info__price" style="right: 35%;">'+
					'<span class="info-label">' + jQuery(".cart_messages .qty").text() + '</span>'+
					'<span>' + item.quantity + '</span>'+
				'</div>'+
			'</div>'+
		'</li>').appendTo(table);
            });
          }
          else {
            jQuery(".header-cart .dropdown-toggle").css({"cursor": "default"});
          }
        }
      });
      if(hide_checkout){
        jQuery('#menu-checkout').hide();
      } else {
        jQuery('#menu-checkout').show();
      }
    }
  }

  cartDropDown();
  
  jQuery('.checkout[nj-click] em span').html(Shopify.formatMoney(cart.total_price, formatMoney));
//  changeHtmlValue(".shopping-cart__total, checkout[nj-click]", Shopify.formatMoney(cart.total_price, formatMoney));
  changeHtmlValue(".bigcounter", cart.item_count);
  jQuery('#my_cart_addlink').attr('href','/cart');

  jQuery('.currency .active').trigger('click');
  

}
  
function cartDropDown() {
if (jQuery(".container ul li.cart__item").length > 0){
    jQuery('.cart__top').show();
  jQuery('.cart__bottom').show();
  jQuery('.cart__top_empty').hide();
  
}
  else {
  jQuery('.cart__top').hide();
  jQuery('.cart__bottom').hide();
  jQuery('.cart__top_empty').show();
  }
}

//Utils
function changeHtmlValue (cell, value) {
  var $cartLinkText = jQuery(cell);
  $cartLinkText.html(value);
};


